# Gruvbox for [Vimium](https://vimium.github.io/)

> A retro theme for [Vimium](https://vimium.github.io/)
> Shamelessly forked from [draculatheme.com/vimium](https://draculatheme.com/vimium).

![Screenshot](./screenshot.png)

## Install

All instructions can be found at [draculatheme.com/vimium](https://draculatheme.com/vimium).

## License

[MIT License](./LICENSE)
